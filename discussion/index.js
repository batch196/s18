// function able to recive data
function printName(name){
	console.log("My name is " + name);
}

printName("Juana");
printName("Zandro");

function printMyAge(age){
	console.log("I am " + age);
}
printMyAge(18);
printMyAge()

function checkDivisibiliyuBy8(num){
	let remainder = num % 8;
	console.log("Tthe remainder is " + num + " divided by 8 is " + remainder);

	let isDivisibleBy8 = remainder === 0;
	console.log('Is ' + num + " divisible by 8?");
	console.log(isDivisibleBy8);

}
checkDivisibiliyuBy8(64);
checkDivisibiliyuBy8(27);

// mini activity

function favoriteSuperhero(hero){
	console.log("My favorite superhero is " + hero);
}
favoriteSuperhero("Deadpool");

function printMyFavoriteLanguage(Language){
	console.log("My favorite language is " + Language);
}

printMyFavoriteLanguage("Javascript");
printMyFavoriteLanguage("Java");

// multiple arguments and multiple parameter

function printFullName(firstName,middleName,lastName){
console.log(firstName + " " + middleName + " " + lastName);
}
printFullName("Juan","Dela", "Cruz");

printFullName("Stephen","Wardell"); //result: undefined last name
printFullName("Stephen", "Wardell", "Curry");
printFullName("Stephen", "Wardell", "Curry", "James");

let fname = "Larry";
let mName = "Joe";
let lName = "Bird";

printFullName(fname,mName,lName);
 
 /*Mini Activity*/
function printFavoriteSongs(song1,song2,song3,song4,song5){
	console.log("My Top 5 Songs");
	console.log("1." + song1);
	console.log("2." + song2);
	console.log("3." + song3);
	console.log("4." + song4);
	console.log("5." + song5);

	return song1;
}
let sample1 = printFavoriteSongs("Tornado of Souls - Megadeth", "Painkiller - Judas Priest", "Symphony of Destruction - Megadeth", "The Trooper - Iron Maiden", "Master of Puppets - Metallica")

console.log(sample1);


//return statement

let fullName = printFullName("Mark", "Joseph","Lacdao")
console.log(fullName); //undefined

function returnFullname(firstName,middleName,lastName){
	return firstName + " " + middleName + " " + lastName;
}
fullName = returnFullname("Ernesto", "Antonio", "Maceda");
console.log(fullName);

console.log(fullName + " is grandpa");

function returnPhilippineAddress(city){
	return city + ", Philippines";
}

let myFullAddress = returnPhilippineAddress("Tudela");
console.log(myFullAddress);
// return a value from a function w/c can we save in a variable

function checkDivisibilityBy4(num){
	let remainder = num % 4;
	let isDivisibleBy4 = remainder === 0;

	return isDivisibleBy4;
	console.log("I am run after the return");
}

let num4isDivisibleBy4 = checkDivisibilityBy4(4);
let num14isDivisibleBy4 = checkDivisibilityBy4(14);

console.log(num4isDivisibleBy4);
console.log(num14isDivisibleBy4);

/* mini activity*/
//debugging

function createPlayerInfo(userName,level,job){
	return "Username:" + userName + ",Level: " + level + ",job: " + job;
}

let user1 = createPlayerInfo("White_night",95,"Paladin");
console.log(user1);















